#!/usr/bin/env elvish
use re;
use str;
use ./libsorcerer src;

fn check {|spell|
	var source-info = [(e:grep -E 'VERSION=[^$]|SOURCE_URL+.*VERSION.*|SPELL=' $spell/DETAILS | cut -d'=' -f2)]

	if (or (< (count $source-info) 3 ) (not (re:match 'https?://' $source-info[2]))) { return; }

	var source = (re:replace '.*(https?://[^/]*/[^/]*/[^/:]*).*' '$1' $source-info[2] | re:replace '\${?SPELL}?' $source-info[0] (one))
	var ver-mask = $source-info[2]
	if (re:match '\${?VERSION}?' $source-info[2]) {
		if (re:match '^https?://' $source-info[2]) {
			set ver-mask = (re:find '\${?VERSION}?[^/:]*' $source-info[2] | put [$source-info[2][0..(all)[end]]][0])
		}
		set ver-mask = (re:replace '.*[/:]([^/:]*)\${?VERSION}?([^:/\.]*)[^:]*' '$1*$2' $ver-mask |
			re:replace '\${SPELL}' $source-info[0] (one) |
			re:replace '^\*$' 'refs/tags/[0-9]*' (one))
	} else {
		set ver-mask = 'refs/tags/[0-9]*'
	}

	# get-version-git may sometimes return nothing
	var src-ver
	try {
		set src-ver = (src:get-version-git $source $ver-mask | re:replace (re:replace 'refs/tags/[0-9]\*|\*' '(.*)' $ver-mask) '$1' (one))
	} catch e { return; }
#	echo (styled 'Checking: '$source-info[0]': '$source-info[1]' -> '$src-ver yellow)
	if (and (not (str:has-prefix $src-ver 'ERR:')) (> (src:ver-cmp $src-ver $source-info[1]) 0)) {
		echo (styled $source-info[0]': '$source-info[1]' -> '$src-ver green bold)
	}
#	if (str:contains $source[0] "github.com") {
#		var src-ver = (src:get-version-gh $source[0])
#		#echo 'Checking: '$source[2]' | '$source[1]' -> '$src-ver
#		#echo (src:ver-cmp $src-ver $source[1])
#		if (> (src:ver-cmp $src-ver $source[1]) 0) {
#			echo (styled $source[2]': '$source[1]' -> '$src-ver green bold)
#		}
#	}


}

each {|x|
	check $x
} [(src:get-grimoire $args[0] | put (one)/*/*/)]
