# Big Awful Feather

> Origianlly was supposed to be called 'quill', but then I found out that it already exists

Set of tools to help working with [SourceMage](https://sourcemage.org) spells

### Status

WIP.

* Update checker script:
	Run using: `./update-checker.elv <grimoire>`
	Will check git repositories of spells in the grimoire for updates (using tags)

	DEPENDS: grep git elvish (>0.18.0)
