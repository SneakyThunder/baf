#!/usr/bin/env elvish

use math;
use str;
use re;

fn get-grimoire {|name|
	put [(str:split = (e:grep $name /etc/sorcery/local/grimoire))][1]
}

fn get-version-gh {|repo|
	set repo = (re:replace '.*github\.com/([^/]*/[^/]*)/.*' '$1' $repo)
	try {
		curl -sL "api.github.com/repos/"$repo"/releases" | from-json | put (all)[0][tag_name]
	} catch e {
		echo "ERR: Cant get version info about: "$repo
	}
}

fn get-version-git {|repo prefix|
	try {
		git ls-remote --sort='-version:refname' --refs --tags $repo $prefix 2> /dev/null | cut -d'/' -f3 | head -n1
	} catch e {
		echo "ERR: Cant get version info about: "$repo
	}
}

fn ver-cmp {|ver-new ver-orig|
	set ver-new =	[(re:replace '[^\d]+' ' ' $ver-new  | str:trim-prefix (all) ' ' | str:split ' ' (all))]
	set ver-orig =	[(re:replace '[^\d]+' ' ' $ver-orig | str:trim-prefix (all) ' ' | str:split ' ' (all))]
	var count = (math:min (count $ver-orig) (count $ver-new))
	for i [(range $count)] {
		#try {
			if (>s $ver-new[$i] $ver-orig[$i]) {
				put (num 1); return
			}
			if (<s $ver-new[$i] $ver-orig[$i]) {
				put (num -1); return
			}
		#} catch e {
		#	if (not (has-key $e[reason] 'type')) {
		#		put (num -2); return
		#	}
		#}
	}
	if (> (count $ver-new) $count) {
		put (num 1); return
	}
	put (num 0); return
}

fn check-updates-gh {|repo ver|
	var gh-ver = (get-version-gh $repo | only-values)
	put (ver-cmp $gh-ver $ver); return
}
